package cn.com.smart.form.controller;

import cn.com.smart.bean.SmartResponse;
import cn.com.smart.filter.bean.FilterParam;
import cn.com.smart.form.bean.entity.TForm;
import cn.com.smart.form.bean.entity.TFormMapping;
import cn.com.smart.form.service.FormImportService;
import cn.com.smart.form.service.FormMappingService;
import cn.com.smart.form.service.FormService;
import cn.com.smart.report.bean.entity.TReport;
import cn.com.smart.web.bean.RequestPage;
import cn.com.smart.web.bean.UserInfo;
import cn.com.smart.web.constant.enums.BtnPropType;
import cn.com.smart.web.helper.HttpRequestHelper;
import cn.com.smart.web.service.OPService;
import cn.com.smart.web.tag.bean.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mixsmart.utils.StringUtils;
import org.snaker.engine.helper.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表单映射控制器
 */
@Controller
@RequestMapping("/form/mapping")
public class FormMappingController extends BaseFormController {
	
	private static final String VIEW_DIR = "form/mapping/";

	@Autowired
	private FormMappingService formMappingServ;
	@Autowired
	private OPService opServ;

	/**
	 * 配置表单映射
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/config")
	public ModelAndView config(HttpServletRequest request, String id) {
		ModelAndView modelView = new ModelAndView();
		if(StringUtils.isNotEmpty(id)) {
			TFormMapping formMapping = formMappingServ.queryAssoc(id);
			if(null != formMapping) {
				modelView.getModelMap().put("objBean", formMapping);
			}
		}
		modelView.setViewName(VIEW_DIR + "config");
		return modelView;
	}
	
	/**
	 * 表单列表
	 * @param request
	 * @param searchParam
	 * @param page
	 * @return
	 */
	@RequestMapping("/list")
	public ModelAndView list(HttpServletRequest request,FilterParam searchParam,RequestPage page) {
		ModelAndView modelView = new ModelAndView();
		SmartResponse<Object> smartResp = opServ.getDatas("form_mapping_mgr_list", searchParam,page.getStartNum(), page.getPageSize());
		String uri = HttpRequestHelper.getCurrentUri(request);

		addBtn = new EditBtn("add","form/mapping/config", null, "创建表单映射", "800");
		editBtn = new EditBtn("edit","form/mapping/config", null, "修改表单映射", "800");
		delBtn = new DelBtn("form/mapping/delete", "确定要删除选中的表单映射关系吗？",uri,null, null);
		refreshBtn = new RefreshBtn(uri, null,null);
		pageParam = new PageParam(uri, null, page.getPage(), page.getPageSize());
		ModelMap modelMap = modelView.getModelMap();
		modelMap.put("smartResp", smartResp);
		modelMap.put("searchParam", searchParam);
		modelMap.put("delBtn", delBtn);
		modelMap.put("addBtn", addBtn);
		modelMap.put("editBtn", editBtn);
		modelMap.put("refreshBtn", refreshBtn);
		modelMap.put("pageParam", pageParam);
		modelView.setViewName(VIEW_DIR+"/list");
		return modelView;
	}

	/**
	 * 保存表单映射数据
	 * @param session
	 * @param formMapping
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces="application/json; chartset=UTF-8")
	@ResponseBody
	public SmartResponse<String> save(HttpSession session, TFormMapping formMapping) {
		SmartResponse<String> smartResp = new SmartResponse<String>();
		UserInfo userInfo = super.getUserInfoFromSession(session);
		formMapping.setUserId(userInfo.getId());
		smartResp = formMappingServ.saveOrUpdate(formMapping);
		return smartResp;
	}

	/**
     * 删除表单
     * @param id
     * @return
     */
    @RequestMapping(value="/delete", produces="application/json;charset=UTF-8")
    @ResponseBody
    public SmartResponse<String> delete(String id) {
        SmartResponse<String> smartResp = new SmartResponse<String>();
        smartResp.setMsg("删除失败");
        if(StringUtils.isNotEmpty(id)) {
            smartResp = formMappingServ.delete(id);
        }
        return smartResp;
    }
	
}
