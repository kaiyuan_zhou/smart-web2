<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/form/js/form.list.js"></script>
<div class="wrap-content cnoj-auto-limit-height">
    <div class="form-list-designer p-5 p-t-0 p-l-0 p-r-1">
        <div class="panel panel-default">
            <div class="panel-heading p-r-0" style="padding-top:7px; padding-bottom: 7px;"><label>列表设置</label>
                <div class="pull-right" style="margin-top: -7px;"><button id="save-form-list-designer" type="button" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> 保存</button></div>
            </div>
            <div class="panel-body" id="form-list-body" >
		        <form method="post" id="form-list-designer-form">
		          <input type="hidden" name="id" value="${objBean.id }" />
                    <input type="hidden" name="report.id" value="${objBean.report.id }" />
		          <input type="hidden" name="report.properties.id" value="${objBean.report.properties.id }" />
		          <input type="hidden" name="report.sqlResource.id" value="${objBean.report.sqlResource.id }" />
                    <input type="hidden" name="isAutoCreate" value="${objBean.isAutoCreate }" />
		          <table class="table table-condensed table-bordered table-sm">
		              <tbody>
		                  <tr>
		                      <th style="width: 80px;">名称</th>
		                      <td>
		                          <input type="text" name="name" data-label-name="名称" class="form-control require" placeholder="请输入列表名称" value="${objBean.name }" />
		                      </td>
                              <th style="width: 80px;">选择表单</th>
                              <td>
                                  <select id="select-form" class="form-control cnoj-select select-form-control" name="formId" data-uri="op/query/select_all_form_list.json" data-default-value="${objBean.formId }" >
                                  </select>
                              </td>
                              <th style="width: 80px;">表单类型</th>
                              <td>
                                  <select id="form-list-type" class="form-control cnoj-select select-form-control" name="type" data-uri="form/list/formListType.json" data-default-value="${objBean.type }" >
                                  </select>
                              </td>
                          </tr>
                          <tr>
                              <th style="width: 70px;">固定标题</th>
                              <td>
                                  <select class="form-control cnoj-select select-form-control" name="report.properties.isFixedHeader" data-uri="dict/item/YES_OR_NO.json" data-default-value="${objBean.report.properties.isFixedHeader==null?'1':objBean.report.properties.isFixedHeader }" >
                                  </select>
                              </td>
                              <th>数据范围</th>
                              <td>
                                  <select class="form-control cnoj-select select-form-control" name="dataScope" data-uri="form/list/dataScope.json" data-default-value="${objBean.dataScope }" >
                                  </select>
                              </td>
                              <th>有ID</th>
                              <td>
                                  <select class="form-control cnoj-select select-form-control" name="report.properties.isHasId" data-uri="dict/item/YES_OR_NO.json" data-default-value="${objBean.report.properties.isHasId==null?'1':objBean.report.properties.isHasId }" >
                                  </select>
                              </td>
                          </tr>
                          <tr>
                              <th>显示ID</th>
                              <td>
                                  <select id="report-type" class="form-control cnoj-select select-form-control" name="report.properties.isShowId" data-uri="dict/item/YES_OR_NO.json" data-default-value="${objBean.report.properties.isShowId==null?'0':objBean.report.properties.isShowId }" >
                                  </select>
                              </td>
                              <th>有复选框</th>
                              <td>
                                  <select class="form-control cnoj-select select-form-control" name="report.properties.isCheckbox" data-uri="dict/item/YES_OR_NO.json" data-default-value="${objBean.report.properties.isCheckbox==null?'0':objBean.report.properties.isCheckbox }" >
                                  </select>
                              </td>
                              <th>列表宽</th>
                              <td>
                                <input type="text" name="report.width" data-label-name="列表宽" class="form-control" placeholder="自动宽度" value="${objBean.report.width }" />
                              </td>
                          </tr>
                          <tr class="bg-color-pd">
                              <td colspan="8">
                                  <div class="col-sm-6 p-t-5 p-b-3 p-l-0 p-r-0 color-pd text-bold">自定义SQL语句</div>
                              </td>
                          </tr>
                          <tr>
                              <th style="width: 80px;">名称</th>
                              <td colspan="7">
                                  <input type="text" name="report.sqlResource.name" class="form-control require" placeholder="请输入SQL语句的名称，注：名称不要重复" value="${objBean.report.sqlResource.name }" />
                              </td>
                          </tr>
                          <tr>
                              <td colspan="8">
                                  <textarea name="report.sqlResource.sql" class="form-control require" style="width: 99%;" rows="5" placeholder="请输入自定义SQL语句">${objBean.report.sqlResource.sql }</textarea>
                              </td>
                          </tr>
                          <tr class="bg-color-pd">
		                    <td colspan="8">
		                       <div class="col-sm-6 p-t-5 p-b-3 p-l-0 p-r-0 color-pd text-bold">字段设置</div>
		                       <div class="col-sm-6 p-t-5 p-b-3 p-r-5 text-right"><button type="button" class="add-form-list-field btn btn-info btn-xs"><i class="glyphicon glyphicon-plus-sign"></i> 添加</button></div>
		                    </td>
		                  </tr>
		                  <tr>
		                     <td colspan="8" class="seamless-embed-table">
                                <div id="field-prop-wrap" style="overflow: auto;height: 250px;">
    		                        <table class="table table-bordered table-condensed table-sm" id="form-list-field-table" style="width: 1800px;">
    		                           <thead>
    		                              <tr class="ui-state-default text-center" style="border: none;">
    		                                 <th style="width: 40px;">序号</th>
    		                                 <th style="width: 150px;">标题</th>
    		                                 <th style="width: 80px;">宽度</th>
    		                                 <th style="width: 300px;">链接URL</th>
    		                                 <th style="width: 100px;">打开方式</th>
    		                                 <th style="width: 120px;">链接参数</th>
    		                                 <th style="width: 120px;">链接参数值</th>
    		                                 <th style="width: 120px;">搜索变量名</th>
                                             <th>搜索插件</th>
                                             <th>插件数据来源</th>
                                              <th>搜索选项值</th>
                                             <th>排序字段名</th>
    		                                 <th style="width: 40px">操作</th>
    		                              </tr>
    		                           </thead>
                                       <tbody>
                                          <c:if test="${not empty objBean && not empty objBean.report && not empty objBean.report.fields }">
                                              <c:forEach items="${objBean.report.fields }" var="field" varStatus="st">
                                                  <tr>
                                                      <td class="seq-num text-right" style="width: 40px;">
                                                          <input type="hidden" name="report.fields[${st.index}].id" value="${field.id }" />
                                                          <input type="hidden" class="sort-order" name="report.fields[${st.index}].sortOrder" value="${field.sortOrder }" />
                                                          <span class="sort-order-label">${field.sortOrder }</span>
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].title" id="title${st.index+1}" class="form-control title" placeholder="请填写标题，必填" title="必填项" type="text" value="${field.title }"  />
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].width" class="form-control" placeholder="自动宽度" type="text" value="${field.width }"  />
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].url" class="form-control" placeholder="URL地址" type="text" value="${field.url }"  />
                                                      </td>
                                                      <td>
                                                          <select class="form-control cnoj-select" name="report.fields[${st.index}].openUrlType" data-uri="dict/item/OPEN_URL_TYPE.json" data-default-value="${field.openUrlType}">
                                                          </select>
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].paramName" class="form-control" title="多个参数用英文逗号分隔，如果没有请为空" placeholder="多个参数用英文逗号分隔，如果没有请为空" type="text" value="${field.paramName }"  />
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].paramValue" class="form-control" title="多个参数引用下标用英文逗号分隔，如果没有请为空" placeholder="多个参数引用下标用英文逗号分隔，如果没有请为空" type="text" value="${field.paramValue }"  />
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].searchName" class="form-control" title="填写搜索变量，如果该标题不是搜索项，请为空" placeholder="填写搜索变量，如果该标题不是搜索项，请为空" type="text" value="${field.searchName }"  />
                                                      </td>
                                                      <td>
                                                          <select name="report.fields[${st.index }].searchPluginType" class="form-control cnoj-select" data-uri="form/list/searchPluginType.json" data-default-value="${field.searchPluginType}">
                                                              <option value="">请选择</option>
                                                          </select>
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].searchPluginUrl" class="form-control" title="填写搜索插件数据来源，如果搜索插件没有数据来源，请为空" placeholder="填写搜索插件数据来源，如果搜索插件没有数据来源，请为空" type="text" value="${field.searchPluginUrl }"  />
                                                      </td>
                                                      <td>
                                                          <textarea name="report.fields[${st.index }].searchCustomOptions" class="form-control field-textarea" title="搜索选项值" placeholder="搜索选项值" cols="1">${field.searchCustomOptions }</textarea>
                                                      </td>
                                                      <td>
                                                          <input name="report.fields[${st.index }].sortFieldName" class="form-control" title="填写按该标题排序的字段名，如果该标题不支持排序，请为空" placeholder="填写按该标题排序的字段名，如果该标题不支持排序，请为空" type="text" value="${field.sortFieldName }"  />
                                                      </td>
                                                      <td class="del-td" id="del-field${st.index+1 }" style="width: 40px" class="text-center">
                                                        <button type="button" title="删除" class="delete-field text-center" style="float: none;font-size: 18px;" data-dismiss="tr1" aria-label="Close"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                      </td>
                                                  </tr>
                                              </c:forEach>
                                          </c:if>
                                       </tbody>
    		                       </table>
                               </div>
		                     </td>
		                  </tr>

                          <tr class="bg-color-pd">
                              <td colspan="8">
                                  <div class="col-sm-6 p-t-5 p-b-3 p-l-0 p-r-0 color-pd text-bold">自定义单元格</div>
                                  <div class="col-sm-6 p-t-5 p-b-3 p-r-5 text-right">
                                      <button type="button" class="add-cell btn btn-info btn-xs"><i class="glyphicon glyphicon-plus-sign"></i> 添加</button>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="8" class="seamless-embed-table">
                                  <div style="overflow: auto;height: 150px;">
                                      <table class="table table-bordered table-condensed table-sm" id="form-list-cell-table">
                                          <thead>
                                          <tr class="ui-state-default text-center" style="border: none;">
                                              <th style="width: 40px;">序号</th>
                                              <th style="width: 150px;">单元格位置</th>
                                              <th>单元格内容</th>
                                              <th style="width: 200px;">参数名称</th>
                                              <th style="width: 200px;">参数值</th>
                                              <th style="width: 40px">操作</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                            <c:if test="${not empty objBean && not empty objBean.report && not empty objBean.report.customCells }">
                                                <c:forEach items="${objBean.report.customCells }" var="cell" varStatus="st">
                                                <tr>
                                                    <td class="seq-num text-right">
                                                        <input type="hidden" name="report.customCells[${st.index}].id" value="${cell.id }" />
                                                        <span class="sort-order-label">${st.index+1 }</span>
                                                    </td>
                                                    <td>
                                                        <input name="report.customCells[${st.index}].position" class="form-control" type="text" placeholder="请输入显示的位置序号，从1开始" value="${cell.position }" />
                                                    </td>
                                                    <td>
                                                        <textarea name="report.customCells[${st.index}].content" style="width: 100%" type="text" class="form-control field-textarea" placeholder="请输入显示的内容模板">${cell.content }</textarea>
                                                    </td>
                                                    <td>
                                                        <input name="report.customCells[${st.index}].paramName" class="form-control" type="text" placeholder="内容模板中出现的参数名，多个用英文逗号分隔" value="${cell.paramName }" />
                                                    </td>
                                                    <td>
                                                        <input name="report.customCells[${st.index}].paramValue" class="form-control" type="text" placeholder="参数名对应值的数据下标，从0开始" value="${cell.paramValue }" />
                                                    </td>
                                                    <td class="del-td" id="del-cell${st.index}" class="text-center">
                                                        <button type="button" title="删除" class="delete-cell text-center" style="float: none;font-size: 18px;" data-dismiss="tr1" aria-label="Close"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                                </c:forEach>
                                            </c:if>
                                          </tbody>
                                      </table>
                                  </div>
                              </td>
                          </tr>

                          <tr class="bg-color-pd">
                            <td colspan="8">
                                <div class="col-sm-6 p-t-5 p-b-3 p-l-0 p-r-0 color-pd text-bold">自定义操作按钮</div>
                                <div class="col-sm-6 p-t-5 p-b-3 p-r-5 text-right"><button type="button" class="add-btn btn btn-info btn-xs"><i class="glyphicon glyphicon-plus-sign"></i> 添加</button></div>
                            </td>
                          </tr>
                          <tr>
                              <td colspan="8" class="seamless-embed-table">
                                  <div id="custom-op-wrap" style="overflow: auto;height: 150px;">
                                    <table class="table table-bordered table-condensed table-sm" id="form-list-btn-table" style="width: 1500px;">
                                        <thead>
                                        <tr class="ui-state-default text-center" style="border: none;">
                                            <th style="width: 40px;">序号</th>
                                            <th style="width: 100px;">按钮名称</th>
                                            <th style="width: 120px;">样式类型</th>
                                            <th style="width: 100px;">操作类型</th>
                                            <th style="width: 200px;">URL</th>
                                            <th style="width: 120px;">数据选中方式</th>
                                            <th style="width: 100px;">打开方式</th>
                                            <th style="width: 100px;">是否制权</th>
                                            <th style="width: 100px;">弹框标题</th>
                                            <th style="width: 100px;">弹框宽度</th>
                                            <th style="width: 100px;">按钮图标</th>
                                            <th style="width: 100px;">参数名</th>
                                            <th style="width: 100px;">提示信息</th>
                                            <th style="width: 40px">操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${not empty objBean && not empty objBean.report && not empty objBean.report.buttons }">
                                            <c:forEach items="${objBean.report.buttons }" var="btn" varStatus="st">
                                                <tr>
                                                    <td class="seq-num text-right">
                                                        <input type="hidden" name="report.buttons[${st.index}].id" value="${btn.id }" />
                                                        <input type="hidden" class="sort-order" name="report.buttons[${st.index}].sortOrder" value="${btn.sortOrder }" />
                                                        <span class="sort-order-label">${btn.sortOrder }</span>

                                                    </td>
                                                    <td>
                                                        <input name="report.buttons[${st.index }].name" class="form-control" placeholder="请填写按钮名称，必填" title="必填项" type="text" value="${btn.name }"  />
                                                    </td>
                                                    <td>
                                                        <select name="report.buttons[${st.index }].btnStyle" class="form-control" value="${btn.btnStyle }">
                                                            <option value="btn-default">默认</option>
                                                            <option value="btn-primary" ${btn.btnStyle == 'btn-primary'?'selected':''} style="background-color: #428bca;">主要</option>
                                                            <option value="btn-success" ${btn.btnStyle == 'btn-success'?'selected':''} style="background-color: #449d44;">成功</option>
                                                            <option value="btn-info" ${btn.btnStyle == 'btn-info'?'selected':''} style="background-color: #5bc0de;">信息</option>
                                                            <option value="btn-warning" ${btn.btnStyle == 'btn-warning'?'selected':''} style="background-color: #f0ad4e;">警告</option>
                                                            <option value="btn-danger" ${btn.btnStyle == 'btn-danger'?'selected':''} style="background-color: #d9534f;">危险</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="report.buttons[${st.index }].btnId" class="form-control cnoj-select" data-uri="opauth/options" data-default-value="${btn.btnId }">
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input name="report.buttons[${st.index }].url" class="form-control" placeholder="点击按钮时触发的URL地址" type="text" value="${btn.url }"  />
                                                    </td>
                                                    <td>
                                                        <select class="form-control cnoj-select" name="report.buttons[${st.index}].selectedType" data-uri="web/enum/btn/selectedListType.json" data-default-value="${btn.selectedType}">
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control cnoj-select" name="report.buttons[${st.index}].openType" data-uri="web/enum/btn/openType.json" data-default-value="${btn.openType}">
                                                        </select>
                                                    </td>

                                                    <td>
                                                        <select class="form-control cnoj-select" name="report.buttons[${st.index}].isAuth" data-uri="dict/item/YES_OR_NO.json" data-default-value="${btn.isAuth}">
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input name="report.buttons[${st.index }].dialogTitle" class="form-control" title="填写弹出框或tab的标题，如果不是弹出框或tab，请为空" placeholder="填写弹出框或tab的标题，如果不是弹出框或tab，请为空" type="text" value="${btn.dialogTitle }"  />
                                                    </td>
                                                    <td>
                                                        <input name="report.buttons[${st.index }].dialogWidth" class="form-control" title="填写弹出框窗口的宽度，如果不是弹出框窗口，请为空" placeholder="填写弹出框窗口的宽度，如果不是弹出框窗口，请为空" type="text" value="${btn.dialogWidth }"  />
                                                    </td>
                                                    <td>
                                                        <input name="report.buttons[${st.index }].btnIcon" class="form-control" title="填写按钮的图标，请使用bootstrap的字体图标，可为空" placeholder="填写按钮的图标，请使用bootstrap的字体图标，可为空" type="text" value="${btn.btnIcon }"  />
                                                    </td>
                                                    <td>
                                                        <input name="report.buttons[${st.index }].paramName" class="form-control" title="填写参数名称，默认为“id”，如果没有选择的值，请为空" placeholder="填写参数名称，默认为“id”，如果没有选择的值，请为空" type="text" value="${btn.paramName }"  />
                                                    </td>
                                                    <td>
                                                        <input type="text" name="report.buttons[${st.index}].promptMsg" class="form-control" value="${btn.promptMsg }" placeholder="用于删除提醒" />
                                                    </td>
                                                    <td class="del-td" id="del-btn${st.index+1 }" class="text-center">
                                                        <button type="button" title="删除" class="delete-btn text-center" style="float: none;font-size: 18px;" data-dismiss="tr1" aria-label="Close"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:if>
                                        </tbody>
                                    </table>
                                  </div>
                             </td>
                          </tr>
		              </tbody>
		           </table>
		        </form>
	        </div>
	    </div>
    </div>
</div>
 <script type="text/javascript">
    //plugins/form/js/form.list.js
    setTimeout("loadJs()", 200);
    function loadJs(){
        $("#field-prop-wrap, #custom-op-wrap").width($("#form-list-body").width());
       $("#form-list-designer-form").formListPropListener();
    }
 </script>